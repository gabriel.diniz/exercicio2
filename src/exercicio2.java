public class exercicio2 {

    public static void main(String[] args) { // metodo publico estatico

        exercicio2(); //método
    }

    public static void exercicio2(){ //método

        int [] nums = new int [3];  // array de inteiros com 3 elementos
        nums[0] = 8;  //valores
        nums[1] = 9;  //valores
        nums[2] = 7;  //valores

        int media1 = 0; // variavel
        for(int i = 0; i < nums.length; i++) { // declara variavel 0, condição i menor que nums.length e incrementa 1
            media1 += nums[i];
        }

        float total1 = (float)media1 / nums.length; // variavel float dividindo pelos elementos do array

        nums[0] = 4;  //valores
        nums[1] = 5;  //valores
        nums[2] = 6;  //valores

        int media2 = 0; // variavel
        for(int i = 0; i < nums.length; i++) {
            media2 += nums[i];
        }

        float total2 = (float)media2 / nums.length; // variavel float dividindo pelos elementos do array

        System.out.println("A média 1 é: "+total1); // imprimir média 1

        System.out.println("A média 2 é: "+total2); // imprimir média 2

        System.out.println("A soma das médias é: "+(total1 + total2)); // imprimir a soma das médias

    }
}
